# smash - Simple Monitoring and Alerting System for Home (ugh)

Smash is a basic monitoring and alerting system for home data centres and
other informal computing environments.  Smash is composed of three
interoperating but independent components:

- The [server](https://gitlab.com/dlek/smash/) exposes a REST API where nodes
  can report one or more statuses.  The server also provides a limited
  dashboard as a basic client.

- The agent (this one) is a simple shell script and library that runs on each
  node to run various checks and report each status to the server.

- The [clients](https://gitlab.com/dlek/smash-clients/) are other programs
  that interact with the server, such as for viewing statuses and
  acknowledging statuses.

The server repository is the primary for the project and contains the overall
documentation.

## Overview

The Agent runs on each host, checking status of things as configured and
reporting to the server.  A sample configuration is provided along with
multiple _checks_, tests to run, and sample configuration for those, where
applicable.

## Checks

A check can be any script or program in any language that produces a small
(one line or shorter) amount of text and an exit status indicating the
severity of the result.  Both are reported on the dashboard and in any
alerts generated, with the severity used in presentation and alerting logic as
appropriate.

The text result should be simple, clear, and short.

## Severity

* 0 - Okay - everything's fine
* 1 - Unknown - cannot get status.  Individual checks which may not be able to
ascertain target state might flag that as a warning, error or unusable 
* 2 - Warning - there is a condition you should be aware of and maybe do
something about, but not necessarily right now
* 3 - Error - something is broken or about to become so, you should definitely fix this
* 4 - Unusable - something is broken to the point that the service can't
service critical functions and might be impacting other systems or services

As with syslog priorities and other such measures, determining what condition
means what severity level depends on multiple factors such as the relative
importance of a service, the risk incurred by the service being in a
particular state, the ability of other systems to absorb some of that risk,
and so on.

One example is the *disk usage check*.  For a `/var` filesystem, where filling
up completely can impede system operation and crash services, a reasonable
mapping of fullness (percent) to severity might be:

* Under 60% - Okay
* At 60% - Warning - definitely start thinking about adding space, cleaning up
existing usage, and/or managing use of the space more effectively through log
rotation or truncation, and so on.
* At 80% - Error - nothing's actually broken yet but things are definitely
going to go wrong if you're not careful.  In this case the error state is not
from the operation of the filesystem itself but that nothing's been done about
the space yet.
* At 100% - Unusable.  Things are failing, failed, and impacting other
services.

On a multiuser system, `/home` may follow a similar pattern, since one user
filling up space to 100% can impact other users' use of the system.  On a
single-user system, perhaps the "Unusable" designation isn't used at all,
because you know you just filled the thing up.

## Agent configuration and operation

The agent is a pretty simple Bash script that calls other programs to run
checks on the system and services (or other systems) and report the results to
the server.  Its usage can be viewed this way:

```
$ smash-agent -h
```

The agent must be registered with the server before use.  If the server is
configured for open registrations, this is done by:

```
$ smash-agent -r -s <URI>
```

By default the agent will decide where to put the file based on whether it's
installed under the root user or not.  It will determine the node name,
generate a random key, summarize the registration details for you and prompt
for confirmation.  On acceptance the agent will register the node and write
the configruration to a file.

If that's all done then we can now run checks and report the results.  This is
done with something like:

```
$ smash-agent os-updates disk-usage
```

This tells the agent to run the `os-updates` check, then the `disk-usage`
check, and report the results.

This is the basic syntax.  Full details are available with `smash-agent -h`.

## Scheduling agent runs

Typically the agent will be scheduled to run via cron or some equivalent.  On
my systems, a basic cron job is:

```
32 * * * * $HOME/bin/agent disk-usage os-updates backups
```

This runs the agent every hour at 32 minutes past, without reporting the
results to the console (just to the server).  (Why 32?  I typically specify the
next minute when setting up the cron job at first, such that I can almost
immediately verify the cron job has worked and the node is properly reporting
its results.  Then I leave it at that as a way of randomly distributing agent
check-ins over the given period.)

In this case the agent is running the `disk-usage`, `os-updates`, and
`backups` checks.

If you have some checks that should be run more or less often than others,
just schedule separate agent runs for those checks.  For example, let's check
bakups every three hours, check for OS updates every hour, and look at the
disk usage twice an hour:

```
0 3,6,9,12,15,18,21 * * * $HOME/bin/smash-agent -h 4h backups
32 * * * * $HOME/bin/smash-agent -x 2h os-updates
15,45 * * * * $HOME/bin/smash-agent -x 20m disk-usage
```

In each case we're also telling the server to expect the next report according
to the schedule plus some grace time for the script to run.

## Other agent runs

It may be useful to run the agent at other times, such as on return from
system suspend or sleep, or on boot or before shutdown.

## Agent installation

Both a tarball and an self-extracting installer script are generated by the
Makefile and should be included in distributions.  The installer script
encapsulates the tarball and on execution will unpack it, run its install
script, and clean up if successful.

To run the installer, `scp` it to the host and execute it.  That's it.

To run the install manually, `scp` the tarball to the host, and then:

```
$ tar xfz smash-agent-v0.5.tgz
$ smash-agent-v0.5/install.sh
```

That's it.  If successful, you will probably want to remove the tarball and
the unpacked directory.
