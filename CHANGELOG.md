# Changelog

## 0.6.2 (2024-02-25) More bug fixes and small updates

- Add timeout capability and default to Agent
- Tighten up messaging
- Add checks for K8s client certificates and unsynced OneDrive files
- os-updates check maintains state and can wait before alarming; improved
  messaging
- Fix compatibility bug in processes check

## 0.6.1 (2023-11-26) Bug fixes and small updates

- Escape newline in check script output
- Skip non-existent tests rather than keel over and die
- k8s check script improvements
- Add support for Linux Mint, Alma Linux
- Reboot-needed detection for RH variants
- Message lists updates for Darwin
- Fix bad URI in register_node

## 0.6 (2023-11-18) New repository and new API

- Split from server repository; re-versioned, sort of, to match
- Using JSON with updated API
- Versioning via make
- Minor improvements to installer

## 0.5 (2023-05-06) Agent installer update

- Agent self-installer
- Updated documentation
- Agent command-line argument fix

## 0.4 (2023-02-20) Improved install

- Agent has an easier install

## 0.3.1 (2022-11-14) Agent bug fix

Fix bug in Agent breaking "-x".

## 0.3 (2022-11-13) Staleness checking & checks update

- Agent can now report how long until next report can be expected
- Configurable thresholds in `time-machine` check

## 0.2 (2022-11-10) Packaging

- Update and improve agent
- Packaging and upload to GitLab registry of agent

## 0.1 (2022-10-26) First agent

- Agent and status check scripts
