#!/bin/sh

# ---------------------------------------------------------------------------
#                                                          string functions
# ---------------------------------------------------------------------------

# join($joinstring, $str1, ...) - join strings together
#
# takes 2+ args:
# $1 - join string
# $2... - list elements to join
join()
{
  join="$1"
  shift

  if [ -n "$1" ]
  then
    str="$1"
  fi
  shift

  while [ -n "$1" ]
  do
    str="${str}${join}${1}"
    shift
  done

  echo "$str"
}

dhms_from_s()
{
  s=$1
  let d=s/60/60/24
  let s2=s-d*60*60*24
  let h=s2/60/60
  let s3=s2-h*60*60
  let m=s3/60
  let s4=s3-m*60
  if [ $d -gt 0 ]
  then
    printf "%dd %dh\n" $d $h
  elif [ $h -gt 0 ]
  then
    printf "%dh %dm\n" $h $m
  elif [ $m -gt 0 ]
  then
    printf "%dm %ds\n" $m $s4
  else
    printf "%ds\n" $s4
  fi
}

# ---------------------------------------------------------------------------
#                                          highest error code state machine
# ---------------------------------------------------------------------------

_rcmax=0

higher()
{
  if [ $1 -gt $2 ]
  then
    _rcmax=$1
  else
    _rcmax=$2
  fi
  return $_rcmax
}

highest()
{
  return $_rcmax
}

exit_with_highest_err()
{
  exit $_rcmax
}

# ---------------------------------------------------------------------------
#                                                    logging/error routines
# ---------------------------------------------------------------------------

unusable()
{
  echo "$1"
  higher 4 $_rcmax
}

error()
{
  echo "$1"
  higher 3 $_rcmax
}

warning()
{
  echo "$1"
  higher 2 $_rcmax
}

unknown()
{
  echo "$1"
  higher 1 $_rcmax
}

info()
{
  echo "$1"
}

# synonym for info()
okay()
{
  info "$1"
}

#
# XXX_final() methods flags that error with the given message and exits with
# the highest error code as status
#

unusable_final()
{
  unusable "$1"
  exit_with_highest_err
}

error_final()
{
  error "$1"
  exit_with_highest_err
}

warning_final()
{
  warning "$1"
  exit_with_highest_err
}

unknown_final()
{
  unknown "$1"
  exit_with_highest_err
}

info_final()
{
  info "$1"
  exit_with_highest_err
}

# synbnym for info_final()
okay_final()
{
  info_final "$1"
}

# ---------------------------------------------------------------------------
#                                                             configuration
# ---------------------------------------------------------------------------

simpleconf()
{
  grep -v '^#' ${0%.sh}.conf
}
