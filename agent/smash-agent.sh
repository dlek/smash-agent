#!/bin/bash

ERR_BAD_SYNTAX=1
ERR_NO_KEY=2

DEFAULT_TIMEOUT=30

# load smash agent library
. $(dirname $0)/libsmash.sh

# initialization
checks_dir=$(dirname $0)/checks
version_string="smash-agent version $VERSION"
timeout=$DEFAULT_TIMEOUT

usage()
{
	cat <<EOF
$version_string

Usage: $(basename $0) [-c conffile] register <server_URI> [-n node]
       $(basename $0) [-c conffile] check [-t timeout] [-x interval] <test> ...
       $(basename $0) [-c conffile] delete <test> ...
       $(basename $0) -h
       $(basename $0) -V

In the first case, registers a node against a Smash server.  In the second,
checks are run and the results reported to the configured server for the
registered node.  In the third, specified tests are deleted for the node.

Subsequent cases are to request usage information (this text) and the version.

When checking, tests may include arguments if properly quoted:
  $(basename $0) check test1 "test2 --arg" test3

Each check is limited to running for 'timeout' seconds (if none specified, the
default timeout is $timeout seconds).

The optional 'interval' indicates to the server and other clients the next
report should be expected before this period has elapsed (expressed as such as
"10m", "8h", "7d", and applies to all checks listed).

EOF
}

log()
{
	# if script it used in interactive terminal, use stdout for messages.
	# This is done by checking whether stdin is tied to a terminal or not
	if [ -t 0 ]
	then
		echo "$@"
	else
		logger -t smash "$@"
	fi
}

# handle general command-line arguments
interval=""
while [ -n "$1" ]
do
	case $1 in
		-h)
			usage
			exit 0
			;;
		-V)
			echo $version_string
			exit 0
			;;
		-c)
			shift
			config="$1"
			;;
		register)
			cmd=$1
			break
			;;
		check)
			cmd=$1
			break
			;;
		delete)
			cmd=$1
			break
			;;
		*)
			echo "Unrecognized: $1"
			usage
			exit -1
			;;
	esac
	shift
done
shift

if [ "$cmd" == "register" ]
then
	while [ -n "$1" ]
	do
		case $1 in
			-n)
				shift
				node="$1"
				;;
			*)
				echo "Unrecognized: $1"
				usage
				exit -1
				;;
		esac
	done

	# use given node name or guess
	if [ -z "$node" ]
	then
		node=$(hostname -s) || fatal $ERR_NO_NODE "Could not determine short host name"
	fi

	# generate key
	key=$(openssl rand -base64 18) || fatal $ERR_NO_KEY "Could not generate key"

	# get server URI
	if [ -z "$uri" ]
	then
		fatal $ERR_NO_SERVER "Please specify server URI via `-s`"
	fi

	# confirm with user
	echo "About to save agent configuration with the following:"
	echo
	echo "Server:    $uri"
	echo "Node name: $node"
	echo "Key:       $key"
	echo
	echo "Enter 'yes' to continue with the registration; anything else will abort."
	read response
	if [ "$response" == "yes" ]
	then
		echo "Okay."
		echo
	else
		echo "No."
		echo
		echo "Alright.  Please fix the hostname, or whatever and try again"
		exit 0
	fi

	# write config
	write_config

	# register node
	name=$node register_node || fatal $ERR_REGISTRATION_FAILED "Registration failed with rc $?"

	exit 0

elif [ "$cmd" == "check" ]
then

	while [ -n "$1" ]
	do
		case $1 in
			-t)
				shift
				timeout="$1"
				;;
			-x)
				shift
				interval="$1"
				;;
			-*)
				echo "Unrecognized: $1"
				usage
				exit -1
				;;
			*)
				break
		esac
		shift
	done

	# read config
	read_config

	# run and register each check
	for checkcmd in "$@"
	do
		# checkcmd may include args: we need to isolate the name of the check itself
		check=${checkcmd%% *}
		fullcall="$checks_dir/$checkcmd"

		# check script exists
		script="$checks_dir/$check"
		if [ ! -x "$script" ]
		then
			log "Could not find $check script (looked for '$script')"
			continue
		fi

		msg=$(timeout $timeout $fullcall)
		rc=$?
		if [ $rc -eq 143 ]	# timeout (killed with TERM)
		then
			rc=1		# unknown
			msg="$msg [timed out]"
		fi
		state=${STATES[$rc]}
		logmsg="$check check ran with exit $rc ($state)"
		if [ -n "$msg" ]
		then
			# convert any newlines to literal string "\n"
			msg="${msg//$'\n'/\\n}"

			logmsg="$logmsg: $msg"
		fi
		log "$logmsg"
		node=$node key="$key" state=${STATES[$rc]} status=$check message="$msg" expect="$interval" register_status
	done

elif [ "$cmd" == "delete" ]
then

	# check arguments
	if [ -z "$1" ]
	then
		echo "Missing tests to delete"
		usage
		exit -1
	fi

	# read config
	read_config

	# fake for now
	for test in "$@"
	do
		node=$node key="$key" status=$test delete_status
	done

else

	echo "Unrecognized command: '$cmd'"
	usage
	exit -1

fi
