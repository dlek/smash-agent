#!/bin/sh

failed_to()
{
	>&2 echo $@
	exit 1
}

# determine where to get everything
basedir=$(dirname $0)

# determine where to put everything
for dir in /usr/local /opt $HOME
do
	if [ -w $dir ]
	then
		target=$dir
		break
	fi
done

echo "Installing in ${target}{/bin,/lib}"
install -d ${target}/lib || failed_to create target lib directory
install -m 644 ${basedir}/libsmash.sh ${target}/lib/ || failed_to install libsmash.lib to $target
install -d ${target}/bin || failed_to create target bin directory
escaped=$(echo $target | sed -e 's/\//\\\//g')
sed -e 's/^\..*libsmash.sh$/. '${escaped}'\/lib\/libsmash.sh/' -e 's/^checks_dir=.*/checks_dir='${escaped}'\/lib\/smash-checks/' ${basedir}/smash-agent.sh > ${basedir}/smash-agent || failed_to create localized agent
install -m 755 ${basedir}/smash-agent ${target}/bin/smash-agent || failed_to install agent to $target/bin
install -d ${target}/lib/smash-checks || failed_to create target lib/smash-checks directory
install ${basedir}/checks/* ${target}/lib/smash-checks || failed_to install agent checks to $target/lib/smash-checks
echo "Success"
