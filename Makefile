SOURCE_DIR	:= .
OUTPUT_DIR	:= dist
VERSION		:= $(shell git describe --tags --match '*.*' 2>/dev/null | sed -E 's/([0-9]+)-g([a-f0-9]{7})/dev-\1/g')
AGENT_DIR	:= $(SOURCE_DIR)/agent
AGENT_SOURCES	:= $(AGENT_DIR)/install.sh $(AGENT_DIR)/smash-agent.sh \
		   $(AGENT_DIR)/libsmash.sh $(AGENT_DIR)/checks/*
AGENT_TARBALL_ROOT := smash-agent-$(VERSION)
AGENT_TARBALL	:= $(OUTPUT_DIR)/smash-agent-$(VERSION).tgz
AGENT_INSTALLER	:= $(OUTPUT_DIR)/smash-agent-$(VERSION)-installer

all: tarball installer

$(OUTPUT_DIR):
	@mkdir -p $(OUTPUT_DIR)

.PHONY: version
version:
	@echo $(VERSION)

.PHONY: tarball
tarball: $(AGENT_TARBALL)

$(AGENT_TARBALL): $(OUTPUT_DIR) $(AGENT_SOURCES)
	@mkdir -p $(AGENT_TARBALL_ROOT)
	@cp -pr $(AGENT_DIR)/* $(AGENT_TARBALL_ROOT)/
	@sed -e 's/VERSION=.*/VERSION=$(VERSION)/' $(AGENT_DIR)/libsmash.sh > $(AGENT_TARBALL_ROOT)/libsmash.sh
	@tar cfz $(AGENT_TARBALL) $(AGENT_TARBALL_ROOT)/*
	@rm -Rf $(AGENT_TARBALL_ROOT)
	@ls $(AGENT_TARBALL)

.PHONY: installer
installer: $(AGENT_INSTALLER)

$(AGENT_INSTALLER): $(AGENT_TARBALL)
	@tmp=`mktemp` ;\
		sed -e 's/SOURCEDIR=agent/SOURCEDIR=$(AGENT_TARBALL_ROOT)/' deploy/installer.pt1 > $$tmp ;\
		cat $(AGENT_TARBALL) | base64 >> $$tmp ;\
		cat $$tmp deploy/installer.pt2 > $(AGENT_INSTALLER) ;\
		rm $$tmp
	@chmod +x $(AGENT_INSTALLER)
	@ls $(AGENT_INSTALLER)
